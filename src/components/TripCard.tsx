import {
  Button,
  Card,
  CardBody,
  Flex,
  Heading,
  Icon,
  Text,
} from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { StarIcon } from "@chakra-ui/icons";

import { useGetTrip } from "../api";
import { TripType } from "../api/types";

export default function TripCard({
  image,
  emissions,
  rating,
  days,
  title,
  id,
  countries,
}: TripType & { id: string }) {
  const navigate = useNavigate();
  const { isStale, refetch } = useGetTrip({ id, enabled: false });

  return (
    <Card minW={[220, 400]} maxW={400} flex={1} borderRadius={12}>
      <CardBody
        textAlign={"center"}
        backgroundImage={`url("${image}")`}
        backgroundSize={"cover"}
        backgroundClip={"content-box"}
        borderRadius={16}
        p={2}
      >
        <Flex
          flexDirection={"column"}
          rowGap={2}
          px={4}
          alignItems={"center"}
          backgroundColor={"blackAlpha.600"}
          borderRadius={10}
        >
          <Flex flexDirection={"column"} rowGap={1} pt={10} pb={4}>
            <Heading fontSize={24} color={"white"}>
              {title}
            </Heading>
            <Text color={"white"} fontSize={12}>
              {countries.length} Countries, {days} days
            </Text>
          </Flex>
          <Button
            colorScheme="blue"
            size="sm"
            mb={6}
            onClick={() => navigate(`/trip/${id}`)}
            onMouseEnter={() => isStale && refetch()}
          >
            Learn more
          </Button>
          <Flex
            p={3}
            justifyContent={"space-between"}
            background={"gray.800"}
            textColor={"white"}
            borderRadius={8}
            w={"94%"}
          >
            <Text fontSize={[12, 14]}>Emissions offset:</Text>
            <Text as={"b"} fontSize={[12, 14]} fontWeight={500}>
              {emissions} kg CO<sub>2</sub>e
            </Text>
          </Flex>
          <Flex
            p={4}
            justifyContent={"space-between"}
            borderTopRadius={8}
            backgroundColor={"white"}
            w={"94%"}
          >
            <Text as={"b"} fontSize={[12, 14]}>
              Trip rating
            </Text>
            <Flex alignItems={"center"} columnGap={["4px", "6px"]}>
              {Array.from({ length: Math.round(rating) }).map((_, idx) => (
                <Icon key={idx} as={StarIcon} color={"yellow.400"} />
              ))}{" "}
              <Text as={"b"} fontSize={[12, 14]}>
                {rating}
              </Text>
            </Flex>
          </Flex>
        </Flex>
      </CardBody>
    </Card>
  );
}
