import { ReactNode } from "react";
import { Center, Spinner } from "@chakra-ui/react";

export function Loading({
  loading,
  children,
}: {
  loading: boolean;
  children: ReactNode;
}) {
  return (
    <>
      {loading ? (
        <Center h={"100%"}>
          <Spinner size={"lg"} />
        </Center>
      ) : (
        children
      )}
    </>
  );
}
