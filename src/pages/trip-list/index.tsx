import { Box, Wrap } from "@chakra-ui/react";
import { useEffect } from "react";
import { useInView } from "react-intersection-observer";

import TripCard from "../../components/TripCard";
import { Loading } from "../../components/Loading";
import { useGetTrips } from "../../api";

export function TripList() {
  const { data, isLoading, isFetching, hasNextPage, fetchNextPage } =
    useGetTrips();
  const { ref, inView } = useInView({
    threshold: 0.5,
  });

  const { pages } = data || { trips: [] };

  useEffect(() => {
    if (inView && !isLoading && !isFetching && hasNextPage) {
      fetchNextPage();
    }
  }, [fetchNextPage, hasNextPage, inView, isFetching, isLoading]);

  return (
    <Loading loading={isLoading}>
      <Wrap spacing={[6, 10]} justify={"center"} align={"center"} minH={"100%"}>
        {pages?.flatMap(({ data }) =>
          data.map(({ attributes, id }) => (
            <TripCard key={id} {...attributes} id={id} />
          ))
        )}
        <Box ref={ref} position={"absolute"} bottom={0} height={"300px"} />
      </Wrap>
    </Loading>
  );
}
