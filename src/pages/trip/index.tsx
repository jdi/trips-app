import {
  AspectRatio,
  Box,
  Card,
  CardBody,
  Divider,
  Flex,
  Heading,
  Icon,
  Image,
  ListItem,
  SimpleGrid,
  Text,
  UnorderedList,
} from "@chakra-ui/react";
import { CalendarIcon, MoonIcon, SunIcon, ViewIcon } from "@chakra-ui/icons";
import { Link, useNavigate, useParams } from "react-router-dom";

import ScrollToTop from "../../components/ScrollToTop";
import { Loading } from "../../components/Loading";
import { useGetTrip } from "../../api";

const advantagesConfig: { icon: typeof SunIcon; cardinal: string }[] = [
  { icon: SunIcon, cardinal: "1st" },
  { icon: ViewIcon, cardinal: "2nd" },
  { icon: CalendarIcon, cardinal: "3rd" },
  { icon: MoonIcon, cardinal: "4th" },
];

export function Trip() {
  const { id } = useParams();
  const { data, isLoading } = useGetTrip({ id });
  const navigate = useNavigate();

  const { title, image, days, emissions, countries } =
    data?.data.attributes || {};

  const { advantages, description, subtitle } =
    data?.included[0].attributes || {};

  return (
    <Loading loading={isLoading}>
      <ScrollToTop />
      <Box>
        <Link
          style={{ textDecoration: "underline" }}
          to={".."}
          onClick={(e) => {
            e.preventDefault();
            navigate(-1);
          }}
        >
          Go back
        </Link>
        <Box mt={[8, 12]} mb={[4, 6]}>
          <Heading fontSize={24}>{title}</Heading>
          <Text fontSize={[12, 14]}>{subtitle}</Text>
        </Box>
        <Flex
          flexDirection={["column", "column", "row"]}
          columnGap={16}
          rowGap={12}
        >
          <Flex flexGrow={1} flexDirection={"column"}>
            <AspectRatio ratio={16 / 9}>
              <Image src={image} borderRadius={14} alt={`Image of ${title}`} />
            </AspectRatio>
            <Heading as="h2" fontSize={[14, 16]} my={[4, 6]}>
              Overview
            </Heading>
            <SimpleGrid columns={[1, 2]} gap={4}>
              {advantages?.map((advantage, idx) => (
                <Flex key={advantage} alignItems={"flex-start"} columnGap={4}>
                  <Box>
                    <Icon boxSize={[4, 6]} as={advantagesConfig[idx].icon} />
                  </Box>
                  <Box>
                    <Text as="b" fontSize={[14, 16]}>
                      {advantagesConfig[idx].cardinal} advantage
                    </Text>
                    <Text fontSize={[12, 14]} color={"gray.500"}>
                      {advantage}
                    </Text>
                  </Box>
                </Flex>
              ))}
            </SimpleGrid>
            <Divider borderColor={"blackAlpha.300"} my={6} />
            <Text>{description}</Text>
          </Flex>

          <Card minW={[200, 300]} h={"fit-content"} borderRadius={14}>
            <CardBody>
              <Heading as="h2" size={"md"} mb={2}>
                {days} days
              </Heading>
              <Text fontWeight={500}>Emissions: {emissions} kg CO2E</Text>
              <Divider my={4} />
              <Text fontWeight={500} mb={2}>
                Countries included:
              </Text>
              <UnorderedList display={"grid"} gridTemplateColumns={"1fr 1fr"}>
                {countries?.map((country) => (
                  <ListItem key={country}>{country}</ListItem>
                ))}
              </UnorderedList>
            </CardBody>
          </Card>
        </Flex>
      </Box>
    </Loading>
  );
}
