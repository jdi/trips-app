import {
  Factory,
  JSONAPISerializer,
  Model,
  belongsTo,
  createServer,
} from "miragejs";
import { faker } from "@faker-js/faker";

import { TripType } from "./types";

const countries = [
  "Portugal",
  "Spain",
  "France",
  "Ireland",
  "Poland",
  "Germany",
  "Belgium",
  "Norway",
  "Sweden",
  "Denmark",
];

// To check whether tests are running on Cypress
const cypress = (window as any).Cypress;

export function makeServer() {
  return createServer({
    serializers: {
      application: JSONAPISerializer,
      trip: JSONAPISerializer.extend({
        include: ["details"],
      }),
    },
    models: {
      trip: Model.extend({
        details: belongsTo("detail"),
      }),
      detail: Model.extend({}),
    },
    factories: {
      trip: Factory.extend<Partial<TripType>>({
        title() {
          return !cypress ? faker.location.city() : "Oslo";
        },
        rating() {
          return faker.number.float({ min: 3, max: 5, precision: 0.1 });
        },
        days() {
          return faker.number.int({ min: 3, max: 21 });
        },
        emissions() {
          return faker.number.int({ min: 200, max: 4000 });
        },
        image() {
          return faker.image.urlLoremFlickr({ category: "city" });
        },
        countries() {
          return countries.slice(0, faker.number.int({ min: 2, max: 9 }));
        },
      }),
      detail: Factory.extend<TripType["details"]>({
        subtitle() {
          return !cypress ? faker.lorem.sentence() : "Oslo is Norway's capital";
        },
        advantages() {
          return Array.from({ length: 4 }).map(() => faker.lorem.paragraph());
        },
        description() {
          return faker.lorem.paragraph(6);
        },
      }),
    },

    routes() {
      this.urlPrefix = "https://chooose.today";
      this.namespace = "api";

      this.get("/trips", function (schema, request) {
        const page = parseInt(request.queryParams.page) || 1;
        const perPage = parseInt(request.queryParams.perPage) || 20;
        const end = perPage * page;
        const start = end - perPage;

        const trips = schema.all("trip").slice(start, end);

        // A function to shadow "this" must be used here
        // as per: https://miragejs.com/docs/main-concepts/serializers/#working-with-serialized-json
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const json = this.serialize(trips);
        json.pagination = {
          nextPage: page < 10 ? page + 1 : undefined,
          total: 100,
        };

        return json;
      });

      this.get("/trips/:id");
    },

    seeds(server) {
      const details = server.create("detail");
      server.createList("trip", 100, {
        details,
      });
    },
  });
}
