import { useInfiniteQuery, useQuery } from "@tanstack/react-query";

import { APICallWithIncluded, APICallWithPagination, TripType } from "./types";

export function useGetTrip({
  id,
  enabled = true,
}: {
  id: string | undefined;
  enabled?: boolean;
}) {
  return useQuery<APICallWithIncluded<TripType, TripType["details"]>>({
    queryKey: ["trip", id],
    queryFn: async () => {
      const res = await fetch(`https://chooose.today/api/trips/${id}`);
      return await res.json();
    },
    enabled,
  });
}

export function useGetTrips() {
  return useInfiniteQuery<APICallWithPagination<TripType>>({
    queryKey: ["trips"],
    queryFn: async ({ pageParam = 1 }) => {
      const res = await fetch(
        `https://chooose.today/api/trips?${new URLSearchParams({
          perPage: "10",
          page: pageParam,
        }).toString()}`
      );
      return await res.json();
    },
    getNextPageParam: (lastPage) => lastPage.pagination.nextPage,
  });
}
