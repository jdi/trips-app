export interface TripType {
  title: string;
  rating: number;
  days: number;
  emissions: number;
  image: string;
  countries: string[];
  details?: {
    subtitle: string;
    advantages: string[];
    description: string;
  };
}

interface Data<T> {
  data: {
    id: string;
    attributes: T;
  };
}

export interface APICallWithPagination<T> {
  data: {
    id: string;
    attributes: T;
  }[];
  pagination: {
    nextPage: number | undefined;
    total: number;
  };
}

export interface APICallWithIncluded<T, R> extends Data<T> {
  included: {
    attributes: R;
  }[];
}
