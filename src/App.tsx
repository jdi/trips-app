import { Box, Flex } from "@chakra-ui/react";

import { Outlet } from "react-router-dom";
import { Navbar } from "./components/Navbar";

function App() {
  return (
    <Flex flexDirection={"column"}>
      <Navbar />
      <Flex background={"gray.200"} justifyContent={"center"}>
        <Box p={[6, 8]} minH={"100vh"} maxW={1400}>
          <Outlet />
        </Box>
      </Flex>
    </Flex>
  );
}

export default App;
