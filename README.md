# Chooose Travel

## How To Run

First you'll need to install the dependencies with `pnpm install`. After that, simply run `pnpm dev` and go to `http://localhost:5173`.

## How To Test

Important Note: You need to have the app running with `pnpm dev` in order to run the tests.

This app has tests in Cypress. As such, you have two options for running them:

- Interactively: run `pnpm cypress:open`, a window from Cypress should appear, you should then click on E2E Testing, pick any of the  browsers in the list, click on Start E2E Testing and then pick the `trips.cy.ts` Spec.
- On the command line: run `pnpm cypress:run`.

## Reasoning And Notes On Development

- The app is constructed using React, TypeScript and Vite.
- The styling is done with Chakra UI. I was particularly impressed by the way in which it handles responsiveness, as I had never seen it done that way and I found it to be quite practical and intuitive!
- For creating a mock API I used [MirageJS](https://miragejs.com/). It offers a very simple way of creating a schema as well as populating multiple records of data to help in development. I also used [Faker](https://fakerjs.dev/) to generate random bits of data for each entity. All trips are re-generated whenever the app is refreshed, this is by design as to not rely on static data, however, while navigating through the app all the data is maintained as expected. I thought of perhaps forcing MirageJS to keep the data but given the scope of the application, I didn't deem it necessary.
- There are two endpoints, one that retrieves all the trips `/api/trips` and another that retrives a single trip `/api/trips/:id`. You can see in the Console from the browser whenever a request happens.
- For caching purposes I used [Tanstack Query](https://tanstack.com/query/latest) (formerly known as React Query) as it offers a convenient way to fetch resources and handle server state.
- When it came to handling the infinite loading of the trips, I used [react-intersection-observer](https://github.com/thebuilder/react-intersection-observer), because it's quite easy to use.
- The tests were implemented in Cypress because I found it more adequate to test the application in a more holistic way. It also made things like testing the infinity loading easier and without having to rely on mocks. I also set up a CI Pipeline that runs the tests whenever there's a push.
- Needless to say, it was a lot of fun! :D
