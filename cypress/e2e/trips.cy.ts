describe("Trips", () => {
  it("shows loading", () => {
    cy.visit("/");

    cy.get(".chakra-spinner").should("be.visible");

    cy.get("body").find("h2").should("have.length", 10);

    cy.get(".chakra-spinner").should("not.exist");
  });

  it("shows trips", () => {
    cy.visit("/");

    cy.get("body").find("h2").should("have.length", 10);
  });

  it("loads more trips when scrolling down", () => {
    cy.visit("/");

    cy.get("body").find("h2").should("have.length", 10);

    cy.scrollTo("bottom");

    cy.get("body").find("h2").should("have.length.above", 10);
  });

  it("navigates to trip details page", () => {
    cy.visit("/");

    cy.get("p").should("not.have.text", "Oslo is Norway's capital");

    cy.get("button").contains("Learn more").click();

    cy.get(".chakra-spinner").should("be.visible");

    cy.get("body").contains("Oslo is Norway's capital");

    cy.get(".chakra-spinner").should("not.exist");
  });

  it("goes back to trips", () => {
    cy.visit("/");

    cy.get("button").contains("Learn more").click();

    cy.get("body").contains("Oslo is Norway's capital");

    cy.get("button").should("not.exist");

    cy.get("a").click();

    cy.get("body").find("h2").should("have.length", 10);
  });

  it("caches details page and trips page", () => {
    cy.visit("/");

    cy.get(".chakra-spinner").should("be.visible");

    cy.get("body").find("h2").should("have.length", 10);

    cy.get("button").contains("Learn more").click();

    cy.get("button").should("not.exist");

    cy.get(".chakra-spinner").should("be.visible");

    cy.get("a").click();

    cy.get("a").should("not.exist");

    cy.get(".chakra-spinner", { timeout: 0 }).should("not.exist");

    cy.get("button").contains("Learn more").click();

    cy.get("button").should("not.exist");

    cy.get(".chakra-spinner", { timeout: 0 }).should("not.exist");
  });
});
